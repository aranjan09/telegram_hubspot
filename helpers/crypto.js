//Checking the crypto module
require('dotenv').config()
const crypto = require('crypto')
const algorithm = 'aes-256-cbc' //Using AES encryption
const key = process.env.KEY
const iv = process.env.IV

//Encrypting text

module.exports = {
  encrypt: async (text) => {
    // let buffiv = iv.toString('hex')
    // let buffkey = key.toString('hex')
    let cipher = crypto.createCipheriv(
      algorithm,
      Buffer.from(key, 'hex'),
      Buffer.from(iv, 'hex')
    )
    let encrypted = cipher.update(text)
    encrypted = Buffer.concat([encrypted, cipher.final()])
    return encrypted.toString('hex')
  },

  // Decrypting text
  decrypt: async (text) => {
    // let buffiv = Buffer.from(iv, 'hex')
    let encryptedText = Buffer.from(text, 'hex')
    let decipher = crypto.createDecipheriv(
      algorithm,
      Buffer.from(key, 'hex'),
      Buffer.from(iv, 'hex')
    )
    let decrypted = decipher.update(encryptedText)
    decrypted = Buffer.concat([decrypted, decipher.final()])
    return decrypted.toString()
  },

  // Text send to encrypt function
}
