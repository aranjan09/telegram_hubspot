const { UserModel } = require('../src/models/user')
const getRefreshToken = require('./useRefreshToken')

module.exports = async (portalId) => {
  let token = await UserModel.findOne({ portalId: portalId })
  if (token) {
    const expired = Date.now() >= token.updatedAt.getTime() + token.expires_in
    if (expired) {
      console.log('Token expired.... generating new one')
      let access_token = await getRefreshToken(portalId)
      return access_token
    } else return token.access_token
  } else return null
}
