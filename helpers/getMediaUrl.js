const { UserModel, MessageModel } = require('../src/models/user')
const axios = require('axios')
const fs = require('fs')

module.exports = {
  getMediaUrl: async (Channel_post) => {
    let file_id
    if (Channel_post.photo) {
      file_id = Channel_post.photo[Channel_post.photo.length - 1].file_id
    } else if (Channel_post.video) {
      file_id = Channel_post.video.file_id
    } else if (Channel_post.audio) {
      file_id = Channel_post.audio.file_id
    } else if (Channel_post.document) {
      file_id = Channel_post.document.file_id
    } else {
      return false
    }

    const user = await UserModel.findOne({
      telegram_channel_id: Channel_post.sender_chat.id,
    })
    if (user) {
      let { portalId, telegrambot_key, telegram_channel_name } = user

      let FetchResp = await axios.get(
        `https://api.telegram.org/${telegrambot_key}/getFile?file_id=${file_id}`
      )
      console.log(FetchResp.data)
      let { file_path } = FetchResp.data.result
      console.log('filePath: ' + file_path)
      let foldername = file_path.split('/')[0]
      let datapath = `${__basedir}/data`

      let ChannelDataPath = `${datapath}/${telegram_channel_name}/`

      //create channel folder
      if (!fs.existsSync(datapath)) {
        fs.mkdirSync(datapath)
      }

      if (!fs.existsSync(ChannelDataPath)) {
        fs.mkdirSync(ChannelDataPath)
      }

      //create folder if not exist
      if (!fs.existsSync(ChannelDataPath + '/' + foldername)) {
        fs.mkdirSync(ChannelDataPath + '/' + foldername)
      }

      //download file

      let FileResp = await axios.get(
        `https://api.telegram.org/file/${telegrambot_key}/${file_path}`,
        {
          responseType: 'arraybuffer',
        }
      )
      let ProdUrl = `${process.env.HOST}/data/${telegram_channel_name}/${file_path}`

      let fileurl = ChannelDataPath + file_path
      console.log('File url: ' + ProdUrl)

      //save file

      fs.writeFile(fileurl, Buffer.from(FileResp.data), (err) => {
        if (err) throw err
        console.log('The file has been saved!')
      })
      return ProdUrl
    }
  },
}
