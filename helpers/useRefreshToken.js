require('dotenv').config()
const queryString = require('querystring')
const axios = require('axios')
const { UserModel } = require('../src/models/user')

module.exports = async (portalId) => {
  const user = await UserModel.findOne({ portalId })
  console.log('rr ', user)
  const refreshTokenProof = {
    grant_type: 'refresh_token',
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    redirect_uri: process.env.REDIRECT_URL,
    refresh_token: user.refresh_token,
  }

  const response = await axios.post(
    'https://api.hubapi.com/oauth/v1/token',
    queryString.stringify(refreshTokenProof)
  )
  //console.log("acc ", response.data.access_token)
  user.access_token = response.data.access_token
  await user.save()
  return response.data.access_token
  //console.log("rr2 ", user)
  // const newJwtToken = await user.generateAuthtoken()
  // console.log("newJwtToken ", newJwtToken)
  // return newJwtToken
}
