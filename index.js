require('dotenv').config()
const express = require('express')
const axios = require('axios')
const queryString = require('querystring')
const cookieParser = require('cookie-parser')
const { UserModel, MessageModel } = require('./src/models/user')
const auth = require('./middlewares/auth')
const getAccessTokenFromDB = require('./helpers/getAccessToken')
const jwt = require('jsonwebtoken')
const path = require('path')
const cors = require('cors')
const moment = require('moment')
const telegramRoute = require('./src/routes/telegram')
let fs = require('fs')
let formidable = require('formidable')
let fetch = require('node-fetch')
const FormData = require('form-data')
global.__basedir = __dirname

const app = express()

app.set('view engine', 'ejs')
app.use(cookieParser())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
// })

// // Routes
// app.use("/oauth-callback", authRoute); // endpoint auth.

// Telegram Routes:
app.use(telegramRoute)

app.use('/data', express.static(path.join(__dirname, '/data')))

app.use(express.static(path.join(__dirname, '/public')))
app.use(cors())
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ limit: '50mb' }))

require('./db/connect_db')

const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const REDIRECT_URL = process.env.REDIRECT_URL
const Scopes = process.env.SCOPES
const authUrl = `https://app.hubspot.com/oauth/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URL}&scope=${Scopes}`

// Like a login route
app.get('/oauth-callback', async (req, res) => {
  //res.send(req.query.code)
  console.log('inside oauth-callback')
  try {
    const authCodeProof = {
      grant_type: 'authorization_code',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      redirect_uri: REDIRECT_URL,
      code: req.query.code,
    }

    const response = await axios.post(
      'https://api.hubapi.com/oauth/v1/token',
      queryString.stringify(authCodeProof)
    )
    const { access_token, refresh_token } = response.data

    console.log('access token ', access_token)

    const details = await axios.get(
      `https://api.hubapi.com/oauth/v1/access-tokens/${access_token}`
    )

    const { hub_id } = details.data
    // const record = await UserModel.findOne({ portalId: hub_id })
    let userDetails = {
      access_token: access_token,
      refresh_token: refresh_token,
      portalId: hub_id,
      expires_in: 1800 * 1000,
    }

    // if (record) {
    //   console.log('UPDATED..')
    let user = await UserModel.findOneAndUpdate(
      { portalId: hub_id },
      userDetails,
      {
        new: true,
        upsert: true,
      }
    )
    console.log(user)
    //   jwtToken = await user.generateAuthtoken()
    // } else {
    //   userObj = new UserModel(userDetails)
    //   await userObj.save()
    let jwtToken = await user.generateAuthtoken()
    // }
    // to encrypt hubspot accesstoken
    res.cookie('authToken', jwtToken, { httpOnly: true, maxAge: 1800 * 1000 })
    res.redirect('/app/cred')
    // res.redirect(`/${hub_id}`)
  } catch (e) {
    console.log(e)
  }
})

app.get('/api/conversation', async (req, res) => {
  console.log(req.query)
  const { portalId } = req.query
  console.log(portalId)

  try {
    const { telegram_channel_id } = await UserModel.findOne({ portalId })
    console.log(telegram_channel_id)
    let messages = await MessageModel.find({ chat_id: telegram_channel_id })
    console.log(messages)
    if (messages.length > 0) {
      // messages= messages.map(msg=>{
      //   if(msg.body)
      //   return msg
      // })
      res.header('Content-Type', 'application/json')
      res.status(200).json({ status: 200, data: messages })
    } else
      res.status(200).json({
        status: 204,
        message:
          'You have not initiated any conversation with this contact, How about sending the first message now?',
      })
  } catch (err) {
    res.status(400).json({
      status: 400,
      message: err.message,
    })
  }
})

app.post('/api/sendMsg', async (req, res) => {
  //   console.log('Hubspot:  ', req.body)
  const portal_id = req.body.portalId
  const message = req.body.message

  const user = await UserModel.findOne({ portalId: portal_id })
  console.log(user.telegrambot_key)
  console.log(user.telegram_channel_id)

  const botkey = user.telegrambot_key
  const channel = user.telegram_channel_id

  // const token = await getAccessTokenFromDB(portal_id)
  // console.log(token.telegrambot_key)

  const url = `https://api.telegram.org/${botkey}/sendMessage`

  const result = await axios.post(url, {
    chat_id: channel,
    text: message,
  })
  //   const result = axios.post(`https://api.telegram.org/bot5324989626:AAGGdvizcNvXNp7u8SUGz1146uQqXXaDED8/sendMessage?chat_id=@testralpha&text=helloTeam`)
  console.log('portal', portal_id)
  console.log('message', message)
  console.log('result', result.message_id)
  const message_id = result.message_id
  // const timelineData = {
  //   eventTypeId: '1151238',
  //   id: event_id,
  //   objectId: objectId,
  //   message: message
  // }
  console.log(`portalID:${portal_id}`)

  const access_token = await getAccessTokenFromDB(portal_id)

  console.log('Access-Token', access_token)

  const time = moment().utcOffset('+05:30').format('MMMM Do YYYY, h:mm A')
  let messageObj = new MessageModel({
    message_id,
    portal_id: portal_id,
    chat_id: channel,
    body: message,
    time: time,
    status: 'Send',
  })

  await messageObj.save()

  console.log('message save and send')

  res.json({ status: 200, mesage: 'Message sent successfully' })
})
app.get('/:portalId/getUser', async (req, res) => {
  const portal_id = req.params.portalId
  let user = await UserModel.findOne({ portalId: portal_id })
  if (user)
    res.status(200).json({ status: 200, data: user })
  else
    res.status(200).json({ status: 404, mesage: "User not found" })
})

app.get('/[0-9]{0,10}', async (req, res) => {
  try {
    if (req.cookies.authToken) {
      const payload = jwt.verify(
        req.cookies.authToken,
        process.env.JWT_SECRET_KEY,
        {
          ignoreExpiration: true,
        }
      )
      // console.log("payload--> ")
      let portalId = payload.portalId
      const access_token = await getAccessTokenFromDB(portalId)

      res.render('home', { authUrl, token: access_token })
    } else res.render('home', { authUrl, token: '' })
  } catch (e) {
    console.log(e.message)
  }
})

app.get('/logout', (req, res) => {
  res.cookie('authToken', 'ajbsd', { httpOnly: true, maxAge: 0 })
  res.send('<h2>Logged out successfully!</h2>')
})

app.post('/api/formdata', async (req, res) => {
  console.log(req.body)
  let Incommingform = new formidable.IncomingForm()

  ///////////////

  Incommingform.parse(req, async function (error, fields, file) {
    const portal_id = fields.portalId

    const user = await UserModel.findOne({ portalId: portal_id })

    const botkey = user.telegrambot_key
    const channelId = user.telegram_channel_id
    let channelname = user.telegram_channel_name
    console.log(file)

    let filetype = file.fileupload.mimetype
    let ReqDataParam = 'document'
    let Telegramfunction = 'sendDocument'
    let foldername = 'documents'
    if (filetype.includes('image')) {
      ReqDataParam = 'photo'
      Telegramfunction = 'sendPhoto'
      foldername = 'photos'
    } else if (filetype.includes('video')) {
      ReqDataParam = 'video'
      Telegramfunction = 'sendVideo'
      foldername = 'videos'
    } else if (filetype.includes('audio')) {
      ReqDataParam = 'audio'
      Telegramfunction = 'sendAudio'
      foldername = 'audios'
    }

    let filepath = file.fileupload.filepath
    let filename =
      'file' + new Date().getTime() + file.fileupload.originalFilename

    let datapath = `${__dirname}/data`
    let channelPath = `${datapath}/${channelname}`
    let folderPath = `${channelPath}/${foldername}`
    let newfilepath = folderPath + '/' + filename

    if (!fs.existsSync(datapath)) {
      fs.mkdirSync(datapath)
    }
    if (!fs.existsSync(channelPath)) {
      fs.mkdirSync(channelPath)
    }
    if (!fs.existsSync(folderPath)) {
      fs.mkdirSync(folderPath)
    }

    fs.rename(filepath, newfilepath, function () {
      //Send a NodeJS file upload confirmation message
      console.log('NodeJS File Upload Success!')
    })
    let readStream = fs.createReadStream(newfilepath)

    // const ws = fs.createWriteStream(`${datapath}/${foldername}/${filename}`)
    // const dataurl = ws.path

    // readStream.pipe(ws)

    let form = new FormData()
    form.append(ReqDataParam, readStream)

    if (fields.caption) form.append('caption', fields.caption)

    try {
      let PhotoRes = await fetch(
        `https://api.telegram.org/${botkey}/${Telegramfunction}?chat_id=${channelId}`,

        {
          method: 'POST',
          body: form,
        }
      )

      let jsonResponse = await PhotoRes.json()

      let dataurl = `https://niswey.aapnainfotech.com/app/data/${channelname}/${foldername}/${filename}`

      console.log(jsonResponse)
      const time = moment().utcOffset('+05:30').format('MMMM Do YYYY, h:mm A')
      let messageObj = new MessageModel({
        message_id: jsonResponse.result.message_id,
        portal_id: portal_id,
        chat_id: channelId,
        body: dataurl,
        caption: fields.caption ? fields.caption : undefined,
        time: time,
        ismedia: true,
        status: 'Send',
      })

      await messageObj.save()
      fs.unlink(filepath, function (err) {
        console.log('Temporary file deleted')
      })

      fs.unlink(newpath, function (err) {
        console.log('Temporary file deleted')
      })

      res.json({ status: 200, message: 'Message sent successfully' })
    } catch (err) {
      res.json({ status: 400, message: err.message })
    }
  })
})

const { request } = require('http')

const port = 3000
app.listen(port, () => {
  console.log(`app running on port ${port}`)
})
