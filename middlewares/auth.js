const jwt = require('jsonwebtoken')
const getNewToken = require('../helpers/useRefreshToken')

module.exports = async (req, res, next) => {
  console.log('INSIDE MIDDLEWARE')

  try {
    const token = req.cookies.authToken
    // console.log("cookie...  ", token)
    // console.log("cookie2...  ", req.cookies)
    if (token) {
      await jwt.verify(token, process.env.JWT_SECRET_KEY, async (err, data) => {
        if (!err) {
          // console.log("data-->", data)
          //req.access_token = data._id
          req.portalId = data.portalId
          req.userId = data.userId
          // const user = await UserModel.findOne({ userId: data._id }) //, 'tokens.token': token
          // if (user)
          //     req.user = user
          // else
          //     throw new Error()
        } else {
          console.log('### ', err.message)
          if (err.name === 'TokenExpiredError') {
            const payload = jwt.verify(token, process.env.JWT_SECRET_KEY, {
              ignoreExpiration: true,
            })
            // console.log("payload--> ")
            req.portalId = payload.portalId
            req.userId = payload.userId
            const newJwtAccessToken = await getNewToken(payload.userId)
            res.clearCookie()
            res.cookie('authToken', newJwtAccessToken, {
              httpOnly: true,
              maxAge: 1800 * 1000,
            })
          }

          //res.status(405).send({ status: 405, msg: err + " Token Expired, login again" })
          //throw new Error(err.message + " Token Expired, login again")
        }
      })
      next()
    } else next()
    // throw new Error("Invalid request")
  } catch (e) {
    res.status(401).send({ msg: e.message + ', Not Authenticated' })
  }
}
