const mongoose = require('mongoose')
const Schema = mongoose.Schema
const jwt = require('jsonwebtoken')

const UserSchema = new Schema(
  {
    access_token: {
      type: String,
    },
    refresh_token: {
      type: String,
      required: true,
    },
    portalId: {
      type: String,
    },
    api_key: {
      type: String,
    },
    expires_in: {
      type: Number,
    },
    telegrambot_key: {
      type: String,
    },
    telegram_channel_id: {
      type: String,
    },
    telegram_channel_name: {
      type: String,
    },
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
)

const EventSchema = new Schema({
  message_id: {
    type: String,
  },
  timeline_event_id: {
    type: String,
  },
  object_id: {
    type: String,
  },
})

const MessageSchema = new Schema(
  {
    message_id: {
      type: String,
    },
    portal_id: {
      type: String,
    },
    ismedia: {
      type: Boolean,
    },
    chat_id: {
      type: String,
    },
    body: {
      type: String,
    },
    caption: {
      type: String,
    },
    status: {
      type: String,
    },
    time: {
      type: String,
    },
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
)
UserSchema.methods.generateAuthtoken = async function () {
  const user = this
  const jwtSecretKey = process.env.JWT_SECRET_KEY
  const payload = {
    _id: user.access_token,
    portalId: user.portalId,
  }
  const token = await jwt.sign(payload, jwtSecretKey, { expiresIn: '1800s' })
  //user.tokens = user.tokens.concat({ token })
  //await user.save()
  return token
}
const UserModel = mongoose.model('User', UserSchema)
const EventModel = mongoose.model('event', EventSchema)
const MessageModel = mongoose.model('message', MessageSchema)

module.exports = { UserModel, EventModel, MessageModel }
