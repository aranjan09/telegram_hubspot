const express = require('express')
const app = express
const router = express.Router()
const { UserModel, EventModel, MessageModel } = require('../models/user')
const axios = require('axios')
const queryString = require('querystring')
const jwt = require('jsonwebtoken')
const getAccessTokenFromDB = require('../../helpers/getAccessToken')
const { getMediaUrl } = require('../../helpers/getMediaUrl')
var uuid = require('uuid')
const moment = require('moment')
const { env } = require('process')

const HOST_NAME = process.env.HOST
const baseurl = process.env.CHAT_BASEURL

// middleware that is specific to this router
router.use((req, res, next) => {
  // console.log(req.body)
  console.log('Time: ', Date.now())
  next()
})

router.get('/cred', async (req, res) => {
  try {
    if (req.cookies.authToken) {
      const payload = jwt.verify(
        req.cookies.authToken,
        process.env.JWT_SECRET_KEY,
        {
          ignoreExpiration: true,
        }
      )
      // console.log("payload--> ")
      let portalId = payload.portalId
      const access_token = await getAccessTokenFromDB(portalId)

      // console.log("contacts ", response.data.results)
      res.render('telegram', {
        token: access_token,
        portalId,
      })
      //res.send("ok")
    } else res.render('home', { authUrl, token: '' })
  } catch (e) {
    console.log(e)
  }
})

router.post('/:portalId/cred', async (req, res) => {
  console.log('inside cred post')
  try {

    let { portalId } = req.params
    let telegrambot_key = req.body.api_key
    let telegram_channel_id = req.body.channel_id
    const chat = await axios.get(
      `https://api.telegram.org/${telegrambot_key}/getChat?chat_id=${telegram_channel_id}`
    )
    console.log('Channel Title', chat.data.result.title)
    let telegram_channel_name = chat.data.result.title
    let tokenDetails = {
      telegrambot_key,
      telegram_channel_id,
      telegram_channel_name,
      portalId,
    }
    let token = await UserModel.findOneAndUpdate(
      { portalId },
      tokenDetails,
      { upsert: true, new: true }
    )
    console.log('token---->', token)
    console.log('host', HOST_NAME)
    const webhookUrl = `${HOST_NAME}/getUpdates`
    console.log(webhookUrl)
    const setwebhookUrl = `https://api.telegram.org/${telegrambot_key}/setWebhook?url=${webhookUrl}`
    try {
      await axios.get(setwebhookUrl)
    } catch (e) {
      console.log(e)
    }
    console.log('Success webhook set.')
    res.json({ status: 200, message: 'Credentials saved!' })
  } catch (e) {
    console.log(e)
    res.json({ status: 404, message: e.message })
  }
})

router.post('/cred', async (req, res) => {
  console.log('inside cred post')
  try {
    if (req.cookies.authToken) {
      const payload = jwt.verify(
        req.cookies.authToken,
        process.env.JWT_SECRET_KEY,
        {
          ignoreExpiration: true,
        }
      )
      // console.log("payload--> ")
      let portalId = payload.portalId
      console.log('CREDENTIALS :==== ', req.body.api_key)
      let telegrambot_key = req.body.api_key
      let telegram_channel_id = req.body.channel_id
      const chat = await axios.get(
        `https://api.telegram.org/${telegrambot_key}/getChat?chat_id=${telegram_channel_id}`
      )
      console.log('Channel Title', chat.data.result.title)
      let telegram_channel_name = chat.data.result.title
      let tokenDetails = {
        telegrambot_key,
        telegram_channel_id,
        telegram_channel_name,
        portalId: req.portalId,
      }
      let token = await UserModel.findOneAndUpdate(
        { portalId: portalId },
        tokenDetails,
        { upsert: true }
      )
      console.log('token---->', token)
      console.log('host', HOST_NAME)
      const webhookUrl = `${HOST_NAME}/getUpdates`
      console.log(webhookUrl)
      const setwebhookUrl = `https://api.telegram.org/${telegrambot_key}/setWebhook?url=${webhookUrl}`
      try {
        await axios.get(setwebhookUrl)
      } catch (e) {
        console.log(e)
      }
      console.log('Success webhook set.')
      res.render('telegramsuccess')
    } else res.render('home', { authUrl, telegramUrl, token: '' })
  } catch (e) {
    console.log(e)
  }
})

router.post('/:portalId/saveToken', async (req, res) => {
  let tokendata = req.body
  let { portalId } = req.params
  console.log(tokendata)
  try {
    let TokenResponse = await UserModel.findOneAndUpdate({ portalId }, tokendata, { new: true, upsert: true })
    console.log(TokenResponse)
    res.status(200).json({ status: 200, token: TokenResponse })
  } catch (err) {
    res.json({ status: 404, error: err.message })
  }

})

router.post('/sendMsg', async (req, res) => {
  //   console.log('Hubspot:  ', req.body)
  const portal_id = req.body.origin.portalId
  const message = req.body.fields.message
  let { objectId } = req.body.object
  const event_id = uuid.v4()

  const user = await UserModel.findOne({ portalId: portal_id })
  console.log(user.telegrambot_key)
  console.log(user.telegram_channel_id)

  const botkey = user.telegrambot_key
  const channel = user.telegram_channel_id

  // const token = await getAccessTokenFromDB(portal_id)
  // console.log(token.telegrambot_key)

  const url = `https://api.telegram.org/${botkey}/sendMessage`

  let eventId = 1151238

  const result = await axios.post(url, {
    chat_id: channel,
    text: message,
  })
  //   const result = axios.post(`https://api.telegram.org/bot5324989626:AAGGdvizcNvXNp7u8SUGz1146uQqXXaDED8/sendMessage?chat_id=@testralpha&text=helloTeam`)
  console.log('portal', portal_id)
  console.log('message', message)
  console.log('result', result.message_id)
  const message_id = result.message_id
  const timelineData = {
    eventTypeId: '1151238',
    id: event_id,
    objectId: objectId,
    message: message,
  }
  console.log(`portalID:${portal_id}`)

  const access_token = await getAccessTokenFromDB(portal_id)

  console.log('Access-Token', access_token)

  const timelineUrl =
    'https://api.hubapi.com/integrations/v1/909893/timeline/event'

  const headers = {
    Authorization: `Bearer ${access_token}`,
  }

  try {
    const result = await axios.put(timelineUrl, timelineData, {
      headers,
    })
  } catch (e) {
    console.log(e)
  }

  console.log('Timeline Updated')
  let eventObj = new EventModel({
    message_id,
    timeline_event_id: event_id,
    object_id: objectId,
  })
  await eventObj.save()
  const time = moment().utcOffset('+05:30').format('MMMM Do YYYY, h:mm A')
  let messageObj = new MessageModel({
    message_id,
    portal_id: portal_id,
    chat_id: channel,
    body: message,
    time: time,
    status: 'Send',
  })

  await messageObj.save()

  console.log('message save and send')

  res.json({ status: 200, message: 'Message sent successfully' })
})

router.get('/conversation', async (req, res) => {
  //console.log('req', req)
  console.log(req.query)
  const { portalId } = req.query
  console.log(portalId)
  try {
    const { telegram_channel_id } = await UserModel.findOne({ portalId })
    console.log(telegram_channel_id)
    let messages = await MessageModel.find({ chat_id: telegram_channel_id })
    console.log(messages)
    if (messages.length > 0) {
      // messages= messages.map(msg=>{
      //     if(msg.body)
      //     return msg
      //   })
      res.header('Content-Type', 'application/json')
      res.status(200).json({ status: 200, data: messages })
    } else
      res.status(200).json({
        status: 204,
        message:
          'You have not initiated any conversation with this contact, How about sending the first message now?',
      })
  } catch (err) {
    res.status(400).json({
      status: 400,
      message: err.message,
    })
  }
})

router.post('/getUpdates', async (req, res) => {
  let ismedia = false
  let mediaurl = ''
  console.log('Getting updates')
  const data = req.body

  if (!data.channel_post.text) {
    console.log('Media message received')
    ismedia = true
    mediaurl = await getMediaUrl(data.channel_post)
  }

  if (data.channel_post.text) {
    console.log('Channel Post Text message', [
      data.channel_post.text,
      data.channel_post.date,
      data.channel_post.chat.id,
      data.update_id,
    ])
  }
  const user = await UserModel.findOne({
    telegram_channel_id: data.channel_post.chat.id,
  })
  if (user) {
    console.log('USer:: --->', user.portalId)
    const messageId = data.update_id
    const portalId = user.portalId
    const time = moment()
      .utc()
      .utcOffset('+05:30')
      .format('MMMM Do YYYY, h:mm A')
    const messageDetails = {
      message_id: messageId,
      portal_id: portalId,
      chat_id: data.channel_post.chat.id,
      body: ismedia ? mediaurl : data.channel_post.text,
      caption: data.channel_post.caption
        ? data.channel_post.caption
        : undefined,
      time: time,
      ismedia,
      status: 'Received',
    }

    const messageObj = new MessageModel(messageDetails)
    await messageObj.save()
  }

  //   console.log(req)
  return res.status(200).send(true)
})

router.get('/crm', async (req, res) => {
  console.log('query', req.query)
  // console.log(req)
  let { portalId } = req.query
  const user = await UserModel.findOne({ portalId })
  console.log(portalId)
  //console.log(user)
  const channel = user.telegram_channel_name
  console.log(' Chat url ', baseurl)
  res.status(200).json({
    primaryAction: {
      type: 'IFRAME',
      width: 900,
      height: 700,
      uri: `${baseurl}/?portalId=${portalId}&channel=${encodeURIComponent(
        channel
      )}`,
      label: 'Show Conversation',
    },
  })
})

// IF not applied will get error [TypeError: Router.use() requires a middleware function but got a Object]
module.exports = router
